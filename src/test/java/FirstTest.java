import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class FirstTest extends BaseTest{

        @Test
        public void testFirst() {
            Petition petition = new Petition(driver);

            assertNotNull(petition.search);

            petition.search.click();
            petition.search.sendKeys("СКОРОЧЕНИЙ європейський РОБОЧИЙ ЧАС");
            petition.search.submit();

            assertNotNull(petition.result);
            String resultString = petition.result.getText();
            assertTrue(resultString.contains("СКОРОЧЕНИЙ європейський РОБОЧИЙ ЧАС"));

            petition.result.click();

            assertNotNull(petition.resultPage);
            assertEquals(petition.resultPage.getText(), "СКОРОЧЕНИЙ європейський РОБОЧИЙ ЧАС — УКРАЇНЦЯМ!");

        }

}
