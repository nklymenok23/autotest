import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


public abstract class BasePage {
    protected WebDriver driver;

    public BasePage (WebDriver currentDriver) {
        PageFactory.initElements(currentDriver, this);
        this.driver = currentDriver;
    }
}
