import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Petition extends BasePage {
    @FindBy(css = ".txt_input.vat")
    public WebElement search;

    @FindBy(css = ".pet_title")
    public WebElement result;

    @FindBy(css = "div.page_left.col-xs-8 h1")
    public WebElement resultPage;


    public Petition (WebDriver driver) {
        super(driver);
    }
}
